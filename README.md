﻿﻿
# Ubuntu Virtualbox - Serverside

## Settings virtualbox and backend
- Download VM from here: https://drive.google.com/file/d/1e4s17q2ENBGHTO6w41yHfPzn6vIUFEgS/view?usp=sharing
- Ubuntu:
	-  User: user, Password: pwd
- Setup the image OVF with the ubuntu distribution in virtualbox
- Change network from NAT to bridge: settings -> network -> bridge
- Start the vm
- Get the ip address of vm and change following scripts:
	- in frontend change ip url from "apiUrl" (src -> _helpers -> config.js
	- or set the ip address of vm to 192.168.0.34 (which is prefigured)

- Start the apache server in VM and check if it is running:
	- sudo systemctl start apache2
	- sudo systemctl status apache2
	

# Login credentials - Frontend
username: user6
password: NewPasswordWeb1

# Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

# Security

### Cross Origin
allowed origins are checked on serverside and are configured inside config.php

### JWT Header Token
- if header jwt token is set in request token is checked with JWT firebase library
- if header is not set only register and login method are allowed on serverside
- JWT Token is set after successfull logging in and sent back to the user
- JWT Token expires after 30 minutes

### Passwords 
Passwords are encrypted with PHP7 build in password_hash function which also generates a salt and verified with password_verify function

### SQL Injection
To prevent SQL injections a DB library (Medoo) was used because it handles db queries with PDO and prepared statements

### XSS
To avoid XSS attacks the v-html component of vue was avoided
Furthermore no click events have been added in pure html (e.g. <button onClick=""></button>)

Additionally user inputs are sanitized on serverside:
``` php
private function sanitize_xss($value) {     
   return htmlspecialchars(strip_tags($value));
}
```

The function does following: 
1. Removes all PHP and HTML tags via strip_tags()
2. Converts all special characters to their HTML-entity equivalents via tmlspecialchars()

