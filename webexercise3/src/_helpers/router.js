import Vue from 'vue'
import Router from 'vue-router'

import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Videos from '../views/Videos.vue'
import External from '../views/ExternalLinks.vue'
import Table from '../views/Table.vue'
import Logout from '../views/Logout.vue'
import Dashboard from '../views/Dashboard.vue'
import { navbarToggler } from '../_helpers';

Vue.use(Router)


export const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/videos',
      name: 'videos',
      component: Videos
    },
    {
      path: '/external',
      name: 'external',
      component: External
    },
    {
      path: '/table',
      name: 'table',
      component: Table
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
})


router.beforeEach((to, from, next) => {
    //public pages -> allowed to see if not logged in
    const publicPages = ['/login', '/register', '/', '/about', '/external', '/logout'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
    
    // redirect to login page if not logged in and trying to access a restricted page
    if (authRequired && !loggedIn) {
      return next('/');
    }

    next();
    //close sidebar
    navbarToggler.toggleSidebar();
})
