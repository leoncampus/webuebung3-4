import { userService } from '../_services';
import { router } from '../_helpers';

const user = JSON.parse(localStorage.getItem('user'));

const loggedInNavitems = [{ name: 'Home',
                            to: '/'},
                            { name: 'About',
                            to: '/about'},
                            { name: 'External',
                            to: '/external'},
                            { name: 'Table',
                            to: '/table'},
                            { name: 'Videos',
                            to: '/videos'},
                            { name: 'Logout',
                            to: '/logout'}];

const navItems = [{ name: 'Home',
                    to: '/'},
                    { name: 'About',
                    to: '/about'},
                    { name: 'External',
                    to: '/external'}];


const state = user
    ? { status: { loggedIn: true }, user , navItems: loggedInNavitems}
    : { status: {}, user: null , navItems: navItems};

const actions = {
    login({ dispatch, commit }, { username, password }) {
        return new Promise(function waitUntilTransitionEnds(resolve) {
        commit('loginRequest', { username });

        userService.login(username, password)
                .then(response =>{
                  if(response.token && response.token.length > 0 ){
                    let u_user = {
                                  username: response.user,
                                  token: response.token
                                }
                    
                    
                    localStorage.setItem('user', JSON.stringify(u_user));
                    commit('loginSuccess', u_user);
                    router.push({ name: 'dashboard'})
                  }
                  resolve(response);
                })
        });
    },

    logout({ commit }) {
        commit('logout');
        localStorage.removeItem('user');
    },

    register({ dispatch, commit }, user) {
        commit('registerRequest', user);
    
        userService.register(user)
            .then(
                user => {
                    commit('registerSuccess', user);
                    router.push('/login');

                    setTimeout(() => {
                        dispatch('alert/success', 'Registration successful', { root: true });
                    })
                },
                error => {
                    console.error(error)
                    commit('registerFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
        state.navItems = loggedInNavitems;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
        state.navItems = navItems;
    },
    registerRequest(state, user) {
        state.status = { registering: true };
    },
    registerSuccess(state, user) {
        state.status = {};
    },
    registerFailure(state, error) {
        state.status = {};
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};