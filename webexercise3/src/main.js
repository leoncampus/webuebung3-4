import Vue from 'vue'
import App from './App.vue'
import { store } from './_store';
import { router } from './_helpers';
import FlashMessage from '@smartweb/vue-flash-message';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.config.productionTip = false


Vue.use(FlashMessage);
Vue.use(BootstrapVue);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
