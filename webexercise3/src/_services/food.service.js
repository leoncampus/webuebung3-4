import { ajax } from '../_services';

export const foodService = {
    getAll,
    search,
    create
};

function getAll(data) {
    return new Promise(function waitUntilTransitionEnds(resolve, reject) {
        ajax.ajaxRequest('POST', {}, '/food')
            .then(response => {
                resolve(response);
            },
            error => {
                reject(error)
            });
    });
}

function create(data){
    return new Promise(function waitUntilTransitionEnds(resolve) {
        ajax.ajaxRequest('PUT', data, '/food')
            .then(function(data){
                resolve(data);
            }).catch(error => {
                resolve(error)
            });
    });
}

function search(searchVal) {
    return new Promise(function waitUntilTransitionEnds(resolve) {
        ajax.ajaxRequest('POST', {searchString: searchVal}, '/food/search')
            .then(function(data){
                resolve(data);
            }).catch(error => {
                resolve(error)
            });
    });
}