import { config } from '../_helpers';
import {authHeader} from '../_helpers';
import Axios from 'axios'

export const ajax = {
    ajaxRequest: ajaxRequest,
    getRequestData: getRequestData
};

function ajaxRequest(verb, data, url){
    return new Promise(function waitUntilTransitionEnds(resolve, reject) {
        let reqData = getRequestData(data);
             
        if(reqData !== false){
            if(verb === 'POST'){
                if(authHeader().length !== 0){
                    Axios.post(`${config.apiUrl}${url}`,
                                reqData,
                                {
                                    headers: {  'Authorization' : 'JWT ' + authHeader(),
                                                'Content-Type': 'application/json' }
                                }).then(response => {
                                    resolve(response.data); 
                                }).catch(error => { 
                                    reject({error:error.response.data});
                                })
                }else{
                    Axios.post(`${config.apiUrl}${url}`, 
                                reqData)
                                .then(response => {
                                    resolve(response.data) 
                                }).catch(error => { 
                                    reject({error:error.response.data});
                                })
                            }
                
            }else if(verb === 'PUT'){
                if(authHeader().length !== 0){
                    Axios.put(`${config.apiUrl}${url}`,
                                reqData,
                                {
                                    headers: {  'Authorization' : 'JWT ' + authHeader(),
                                                'Content-Type': 'application/json' }
                                }).then(response => {
                                    resolve(response.data) 
                                }).catch(error => { 
                                    reject({error:error.response.data});
                                })
                }
            }else{
                return Promise.reject('VERB ' + verb + ' not allowed.');
            }
        }else{
            return Promise.reject('Request too big.');
        }

    });
    
}

function getRequestData(data){
    let u_data = JSON.stringify(data);
    let u_request = [];
    // Replace the euro sign to escaped unicode representation...
    u_data = u_data.replace(new RegExp(String.fromCharCode(8364),'g'),'\\u20ac');
    if (u_data.length > 0 && u_data.length <= 70000) {
        let u_reqJSON = {
           'n' : u_data.length,
           'd' : u_data.substring(0, 70000)
        };

        u_request.push(u_reqJSON);
        return JSON.stringify({ "jsonReq" : u_request});
    }else{
        return false;
    }
}
