import { ajax } from '../_services';

export const userService = {
    login,
    logout,
    register
};

function login(username, password) {
    let u_data = {username: username,
                  password: password
                 }

    return new Promise(function waitUntilTransitionEnds(resolve) {
        ajax.ajaxRequest('POST', u_data, '/login')
            .then(function(data){
                resolve(data);
            }).catch(error => {
                resolve(error)
            });
    });
}

function logout() {
    return new Promise(function waitUntilTransitionEnds(resolve) {
        localStorage.removeItem('user');
        resolve();
    });
    
}

function register(user) {
    return new Promise(function waitUntilTransitionEnds(resolve) {
        ajax.ajaxRequest('POST', user, '/register')
            .then(function(data){
                resolve(data);
            }).catch(error => {
                resolve(error)
            });
    });
}
