# Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
``

# Backend

## Congigurations

### config.php
set DB values inside config.php 
```
$config->add("DB_TYPE", "mysql");
$config->add("DB_NAME", "webengineering");
$config->add("DB_SERVER_ADDRESS", "127.0.0.1");
$config->add("DB_USER", "root");
$config->add("DB_PASSWORD", "Ct6d!0e0AKr$87fm");

```

set allowed origins (port of frontend client)

```
$config->add("ALLOWED_ORIGINS", array(
    'http://localhost:8080', 
));
```

# Security

##Cross Origin
allowed origins are checked on serverside and are configured inside config.php

##JWT Header Token
- if header jwt token is set in request token is checked with JWT firebase library
- if header is not set only register and login method are allowed on serverside
- JWT Token is set after successfull logging in and sent back to the user
- JWT Token expires after 30 minutes

##Passwords are encrypted with PHP7 build in password_hash function which also generates a salt

##SQL Injection
To prevent SQL injections a DB library (Medoo) was used because it handles db queries with PDO and prepared statements

##XSS
To avoid XSS attacks the v-html component of vue was avoided
Furthermore no click events have been added in pure html (e.g. <button onClick=""></button>)
