<?php
require_once('Autoloader.php');
require_once('vendor'.DIRECTORY_SEPARATOR.'autoload.php');

$dir = __DIR__.DIRECTORY_SEPARATOR.'lf'.DIRECTORY_SEPARATOR;

// Register namespaces here:
(new Autoloader($dir))->registerNamespaces([
    'db'                       => 'database',
    'dependencyService'        => 'dependencyService',
    'config'                   => 'config',
    'food'                     => 'food',
    'food_req'                 => 'food'.DIRECTORY_SEPARATOR.'req',
    'user'                     => 'user',
    'user_req'                 => 'user'.DIRECTORY_SEPARATOR.'req',
    'contracts'                => 'contracts',
    'contracts_request'        => 'contracts'.DIRECTORY_SEPARATOR.'request',
    'helper'                   => 'helper',
    'helper_req'               => 'helper'.DIRECTORY_SEPARATOR.'req',
    'router'                   => 'router',
    'system'                   => 'system',
    'system_req'               => 'system'.DIRECTORY_SEPARATOR.'req',
    
])->register();

$config = lf\config\Config::getInstance();

$config->add("DB_TYPE", "mysql");
$config->add("DB_NAME", "webengineering");
$config->add("DB_SERVER_ADDRESS", "127.0.0.1");
$config->add("DB_USER", "root");
$config->add("DB_PASSWORD", "Webexercise2019");

$config->add("ALLOWED_ORIGINS", array(
    'http://localhost:8080', //change in production
));

//secret for jwt token generation
$config->add("TOKEN_SECRET", "yJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3Mi");

$reqURI = filter_input(INPUT_SERVER, 'REQUEST_URI');


$config->add("LOGGING_BLACKLIST", ""); // A comma seperated list with format: Object|Method

$router = \lf\router\Router::getInstance();

/* 
 * router request paths
 * public function post(string $path, string $obj, string $method)
 */
$router
       
       ->post("login","lf\\user\\req\\User","login")
       ->post("register","lf\\user\\req\\User","register")
        
       ->post("food","lf\\food\\req\\Food","readAll")
       ->put("food","lf\\food\\req\\Food","create")
       ->post("food/search","lf\\food\\req\\Food","search")
        ;

