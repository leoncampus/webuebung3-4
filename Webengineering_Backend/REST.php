<?php

//require_once('cookieSettings.php');
require_once('config.php');
require "vendor/autoload.php";

use \Firebase\JWT\JWT;
use lf\config\Config;
use lf\helper\JSON;

// array holding allowed Origin domains
$config = Config::getInstance();
$allowedOrigins = $config->get("ALLOWED_ORIGINS");

if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
  foreach ($allowedOrigins as $allowedOrigin) {
    if (preg_match('#' . $allowedOrigin . '#', $_SERVER['HTTP_ORIGIN'])) {
      header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Max-Age: 3600'); 
      break;
    }
  }
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        echo $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'];
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

$jsonIn = json_decode(file_get_contents('php://input'));

if($jsonIn != null) {
    $reqData = new lf\helper\RequestData($jsonIn, getallheaders());

    $data = $reqData->getData();
    $path = implode("/",explode('/', trim(filter_input(INPUT_SERVER, 'PATH_INFO'),'/')));
    
    $authorized = false;
    //request was sent with header
    $jsonToken = new JSON();
    if(strlen($reqData->getSession()) > 0) {
        try {
            $decoded = JWT::decode($reqData->getSession(), $config->get("TOKEN_SECRET"), array('HS256'));
            $userSession = new lf\system\Session($reqData->getSession());
            
            $authorized = true;
        }catch (Exception $e){
            $userSession = new lf\system\Session();
            $jsonToken->add('error', 'Access denied.');
            $jsonToken->add('messages', $e->getMessage());
        }
    }else{//no header set
        $userSession = new lf\system\Session();
    }
    
    $verb = $_SERVER['REQUEST_METHOD'];
    $router = \lf\router\Router::getInstance();
    $result = $router->resolve($verb, $path);
    
    //header is not needed to do register or login
    if(isset($result->object) && isset($result->method) && strlen($result->object) > 0 && strlen($result->method) > 0){
        if($result->object == 'lf\\user\\req\\User' && ($result->method == 'register' || $result->method == 'login')){
            $authorized = true;
        }
    }else if($jsonToken->has('error')){//not register or login and we have a token error
        $authorized = false;
    }else if(strlen($userSession->getSessionID()) == 0 || strlen($reqData->getSession()) == 0){//we have a valid token
        $authorized = true;
    }
    
    if($authorized == true){
        if(isset($result->object) && isset($result->method) && strlen($result->object) > 0 && strlen($result->method) > 0 ) {
            if($result->has('data')) {
                $data = $result->getJSON('data')->stringify();
            }
            
            echo lf\dependencyService\DynamicInvocation::invoke(
                    $result->object, 
                    $result->method, 
                    $data,
                    $userSession);
                    
        } else {
            echo '{}';
        }
    }else{
        error_log('No valid session - authentication needed for request.');

        header('WWW-Authenticate: LF');
        header('HTTP/1.0 401 Unauthorized');
        header('Status: 401');

        echo $jsonToken->stringify();
        return;
    }
} else {
    echo '{}';
}
