<?php
namespace lf\contracts\request;

use lf\database\DB;
use lf\system\Session;

abstract class Request {
    /**
     * @var \lf\database\QueryBuilder\QueryBuilderHandler
     */
    protected $db;
    protected $session;
    
    public function __construct(Session $userSession) {
        //initialize db connection
        $db = new DB();
        $this->db = $db->getInstance();
        $this->session = $userSession;
    }
    
}
