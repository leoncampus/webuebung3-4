<?php
namespace lf\database;
// Make sure you have Composer's autoload file included
require 'vendor/autoload.php';
// Using Medoo namespace
use Medoo\Medoo;
use lf\config\Config;

final class DB {
    private $database = null;
    protected $session = '';
    
    public function __construct($servernameParam = '', $dbnameParam = '', $usernameParam = '', $passwordParam = '', $tblprefixParam = '') {
        $config = Config::getInstance();
        
        // Initialize
        $this->database = new Medoo([
            'database_type' => $config->get("DB_TYPE"),
            'database_name' => $config->get("DB_NAME"),
            'server' => $config->get("DB_SERVER_ADDRESS"),
            'username' => $config->get("DB_USER"),
            'password' => $config->get("DB_PASSWORD")
        ]);
    }
    
    public function getInstance() {
        return $this->database;
    }
}