<?php

namespace lf\dependencyService;

use lf\helper\JSON;
use lf\contracts\request\Request;
use lf\system\Session;
use lf\database\DB;

class DynamicInvocation {
    public static function invoke(string $obj, string $method, $data, Session $session) : string {
        // Logging
        $config = \lf\config\Config::getInstance();
        $blacklist = explode(",", $config->get("LOGGING_BLACKLIST"));
        

        // Invocation
        $instance = new $obj($session);

        // Ony allow to instanciate and invoke methods from request-objects - security concern
        if($instance instanceof Request) {
            $jsonOut = $instance->{$method}(new JSON($data));
        }

        if($jsonOut instanceof \lf\helper\JSON) {
            return $jsonOut->stringify();
        } else {
            return '{}';
        }
    }
}
