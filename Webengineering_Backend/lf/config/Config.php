<?php

namespace lf\config;

final class Config {
    /**
     * Call this method to get singleton
     *
     * @return \lf\config\Config
     */
    public static function getInstance() : Config {
        static $inst = null;
        if ($inst === null) {
            $inst = new Config();
        }
        return $inst;
    }
    
    /**
     * Private, so nobody else can instance it
     */
    private function __construct() { }
    private function __clone() { }
    
    public function add(string $key, $value) {
        $this->{$key} = $value;
    }
    
    public function get(string $key) {
        return $this->{$key};
    }
}
