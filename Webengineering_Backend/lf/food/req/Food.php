<?php

namespace lf\food\req;

use lf\helper\JSON;
use lf\contracts\request\Request;

class Food extends Request {
    
    //search for specific string in sweets name table
    public function search(JSON $jsonIn): JSON{
        $json = new JSON();
        
        if(strlen($this->session->getID()) > 0){
            if($jsonIn->has('searchString') && strlen($jsonIn->getStr('searchString')) > 0){
                $records = $this->db->select("lf_sweets", "*", [
                                        "name[~]" => $jsonIn->getStr('searchString')
                                ]);
            }else{
                $records = $this->readAllSweets();
            }
            
            $json->add('records', $records);
        }
        
        return $json;
    }
    
    public function readAll(JSON $jsonIn): JSON{
        $json = new JSON();
        
        if(strlen($this->session->getID()) > 0){
            $json->add('records', $this->readAllSweets());
        }
        return $json;
    }
    
    private function sanitize_xss($value) {     
       return htmlspecialchars(strip_tags($value));
    }
    
    public function create(JSON $jsonIn): JSON{
        $json = new JSON();
        
        if(strlen($this->session->getID()) > 0){
            if($jsonIn->has('calories') && strlen($jsonIn->getStr('calories')) > 0 && 
                $jsonIn->has('name') && strlen($jsonIn->getStr('name')) > 0){
                
                //do insert
                $this->db->insert("lf_sweets", [
                                    "name" => $this->sanitize_xss($jsonIn->getStr('name')),
                                    "calories" => $this->sanitize_xss($jsonIn->getStr('calories')),
                            ]);
                
                if($this->db->id() > 0){
                    $json->add('success', 'Data successfully saved.');
                    $json->add('record', $jsonIn);
                    return $json;
                }
            }
        }
        
        $json->add('error', 'Data could not be saved.');
        return $json;
    }
    
    private function readAllSweets(){
        $data = $this->db->select('lf_sweets', [
                'name',
                'calories'
            ]);
        
        if(count($data) > 0){
            return $data; 
        }
        return []; 
    }
}
