<?php
namespace lf\router;

use lf\helper\JSON;

class Router {
    private $routes = [];
    
    /**
     * Call this method to get singleton
     *
     * @return \lf\router\Router
     */
    public static function getInstance() : Router {
        static $inst = null;
        if ($inst === null) {
            $inst = new Router();
        }
        return $inst;
    }
    
    /**
     * Private, so nobody else can instance it
     */
    private function __construct() { }
    private function __clone() { }
    
    public function add(string $verb, string $path, string $obj, string $method) {
        $verb = strtoupper($verb);
        
        $allowedVerbs = array('GET', 'POST', 'PUT', 'DELETE', 'PATCH');
        if (!in_array($verb, $allowedVerbs)) {
            throw new Exception($verb . ' is not a known verb.', 2);
        }
        
        $this->routes[] = array("path" => $verb.'@'.$path,
                                "object" => $obj,
                                "method" => $method);
        
        return $this;
    }
    
    public function post(string $path, string $obj, string $method) {
        $this->add("POST", $path, $obj, $method);

        return $this;
    }
    public function put(string $path, string $obj, string $method) {
        $this->add("PUT", $path, $obj, $method);

        return $this;
    }
    public function delete(string $path, string $obj, string $method) {
        $this->add("DELETE", $path, $obj, $method);

        return $this;
    }
    public function patch(string $path, string $obj, string $method) {
        $this->add("PATCH", $path, $obj, $method);

        return $this;
    }
    
    public function resolve(string $verb, string $path) : JSON {
        $searchPath = strtoupper($verb).'@'.$path;

        $result = array_filter($this->routes,
                               function ($key) use ($searchPath) {
                                   return ($key["path"] == $searchPath);
                               });

        $ret = new JSON();

        if(count($result) == 1) {
            reset($result); // sets the pointer to the first element of the array
            $first_key = key($result); // returns the keyname of the actual pointing element
            return new JSON($result[$first_key]);
        } else {
            return $ret;
        }
    }
}
