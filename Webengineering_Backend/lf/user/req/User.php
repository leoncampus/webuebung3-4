<?php

namespace lf\user\req;

require "vendor/autoload.php";

use \Firebase\JWT\JWT;
use lf\helper\JSON;
use lf\contracts\request\Request;
use lf\config\Config;

class User extends Request {
    
    //registers a user - creates user db record
    public function register(JSON $jsonIn): JSON{
        $result = new JSON();
        
        if($jsonIn->has('username') && strlen($jsonIn->getStr('username')) > 0 &&
            $jsonIn->has('password') && strlen($jsonIn->getStr('password')) > 0 && 
                $jsonIn->has('email') && strlen($jsonIn->getStr('email')) > 0){
            
            //check if username OR email already exists
            $records = $this->db->select("lf_user", ["username"], [
                                "OR" => [
                                        "username" => $jsonIn->getStr('username'),
                                        "email" => $jsonIn->getStr('email')
                                ]   
                             ]);
            
            //good
            if(count($records) == 0){
                //hash and salts password
                $hashedPwd = password_hash($jsonIn->getStr('password'), PASSWORD_DEFAULT);
                
                //do insert
                $this->db->insert("lf_user", [
                                    "username" => $jsonIn->getStr('username'),
                                    "email" => $jsonIn->getStr('email'),
                                    "password" => $hashedPwd
                            ]);
                
                //create successful
                if($this->db->id() > 0){
                    $result->add('success', 'Registration successful. Welcome ' . $jsonIn->getStr('username'));
                    return $result;
                }
            }else{//email or user exists
                $result->add('error', 'Username ' . $jsonIn->getStr('username') . ' or email is already taken.');
                return $result;
            }
        }
        $result->add('error', 'Registration unsuccessful.');
        return $result;
    }
    
    //checks if user entered correct credentials and if user exists
    //if so returns a JWT Token
    public function login(JSON $jsonIn): JSON{
        $json = new JSON();
        
        //check if no session already running
        if(strlen($this->session->getSessionID()) == 0) {
            //check if username and password set
            if($jsonIn->has('username') && strlen($jsonIn->getStr('username')) > 0 &&
                $jsonIn->has('password') && strlen($jsonIn->getStr('password')) > 0){
                
                //get user by username
                $records = $this->db->select("lf_user", ["username", "email", "password", "ID"], [
                                            "username" => $jsonIn->getStr('username')
                                ]);
                
                //user exists check if pwd correct
                if(count($records) == 1){
                    //check if hashed and salted pwd verifies with given password
                    if(password_verify($jsonIn->getStr('password'), $records[0]["password"])){
                        $token = $this->generateAuthToken($records[0]);

                        $json->add('token', $token);
                        $json->add('success', 'Login successful!');
                        $json->add('user', $records[0]['username']);
                    }else{
                        $json->add('error', 'Password wrong.');
                    }
                }else{
                    $json->add('error', 'User not found.');
                }
            }else{
                $json->add('error', 'Username or Password missing.');
            }
            return $json;
        }
        return $json;
    }
    
    //generates an JWT Token with stored secret and expire date
    private function generateAuthToken($userRecord){
        $config = Config::getInstance();
        
        $secret_key = $config->get("TOKEN_SECRET");
        $issuedat_claim = time(); // issued at
        $notbefore_claim = $issuedat_claim + 2; //not before in seconds
        $expire_claim = $issuedat_claim + 1800; // expire time in seconds - eg 30 minutes
        $token = array(
            "iat" => $issuedat_claim,
            "nbf" => $notbefore_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => $userRecord['ID'],
                "username" => $userRecord['username'],
                "email" => $userRecord['email']
        ));
        
        $jwt = JWT::encode($token, $secret_key);
        return $jwt;
    }
}