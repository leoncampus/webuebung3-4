<?php
namespace lf\helper;

class JSON implements \Iterator {
    private $ITERATEABLE_KEYS = [];
    private $ITERATEABLE_POSITION = 0;
    
    public function __construct($json = false) {
        if($json) {
            if(is_string($json)) {
                $this->setAllMyPublicVariables(json_decode($json, true));
            } else if(is_array($json)) {
                $this->setAllMyPublicVariables($json);
            }
        }
        
        $this->ITERATEABLE_POSITION = 0;
    }

    public function exsists(string $key) : bool {
        return $this->has($key);
    }
    public function has(string $key) : bool {
        if(array_key_exists($key, $this) === true) {
            return true;
        } else {
            //error_log("JSON: $key does not exist");
            return false;
        }
    }
    
    public function isJSON(string $key) : bool {
        if($this->{$key} instanceof JSON) {
            return true;
        } else {
            //error_log("JSON: $key is not an array");
            return false;
        }
    }
    
    public function isArray(string $key) : bool {
        return is_array($this->{$key});
    }

    public function getArray(string $key) : array {
        if($this->isArray($key)) {
            return $this->{$key};
        }
        
        return array();
    }
    public function getJSON(string $key) : JSON {
        if($this->has($key) && $this->isJSON($key)) {
            return $this->{$key};
        }
        
        return new JSON();
    }
    public function getStr(string $key) : string {
        if($this->has($key)) {
            if(is_string($this->{$key})) {
                return $this->{$key};
            } else {
                if(is_numeric($this->{$key})) {
                    return ''.$this->{$key};
                } else {
                    error_log("JSON: $key is not a string");
                }
            }
        }
        return "";
    }
    public function getNum(string $key) {
        if($this->has($key)) {
            if(is_numeric($this->{$key})) {
                return $this->{$key};
            } else {
                error_log("JSON: $key is not a number");
            }
        }
        return 0;
    }
    public function getInt(string $key) :int {
        if($this->has($key)) {
            if(is_int($this->{$key})) {
                return $this->{$key};
            } else {
                error_log("JSON: $key is not an integer");
            }
        }
        return 0;
    }
    public function getDouble(string $key) : double {
        if($this->has($key)) {
            if(is_double($this->{$key})) {
                return $this->{$key};
            } else {
                error_log("JSON: $key is not a decimal number");
            }
        }
        return 0.0;
    }
    
    public function add(string $key, $value) {
        if(is_array($value)) {
            $this->setAllMyPublicVariables([$key => $value]);
        } else {
            $this->{$key} = $value;
            if(in_array($key, $this->ITERATEABLE_KEYS) == false) {
                $this->ITERATEABLE_KEYS[] = $key;
            }
        }
    }
    public function set(string $key, $value) {
        if($this->has($key)) {
            if(is_array($value)) {
                $this->setAllMyPublicVariables([$key => $value]);
            } else {
                $this->{$key} = $value;
            }
        }
    }
    
    public function remove(string $key) {
        if($this->has($key)) {
            if (($idx = array_search($key, $this->ITERATEABLE_KEYS)) !== false) {
                unset($this->ITERATEABLE_KEYS[$idx]);
            }
            unset($this->{$key});
        }
    }
    
    public function stringify($options = JSON_NUMERIC_CHECK) : string {
        return json_encode($this, $options);
    }
    
    private function has_string_keys(array $array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

    private function setAllMyPublicVariables($data) {
        foreach ($data as $key => $value) {
            if($key != 'ITERATEABLE_KEYS' && $key != 'ITERATEABLE_POSITION') {
                if($value instanceof \lh\helper\JSON) {
                    $jsonStr = $value->stringify();
                    $sub = new JSON($jsonStr);
                    $value = $sub;
                } else if (is_array($value)) {
                    if($this->has_string_keys($value) == true) {
                        // This array is an associative arrays --> Convert to JSON
                        $sub = new JSON();
                        $sub->setAllMyPublicVariables($value);
                        $value = $sub;
                    } else {
                        // We need this for JSONs inside arrays or mixed arrays
                        foreach($value as $k => $v) {
                            if(is_array($v) && $this->has_string_keys($v) == true) {
                                $sub = new JSON();
                                $sub->setAllMyPublicVariables($v);
                                $value[$k] = $sub;
                            }
                        }
                    }
                }
                
                $this->{$key} = $value;
                $this->ITERATEABLE_KEYS[] = $key;
            }
        }
    }

    public function current() {
        return $this->{$this->ITERATEABLE_KEYS[$this->ITERATEABLE_POSITION]};
    }

    public function key(): \scalar {
        return $this->ITERATEABLE_POSITION;
    }

    public function next(): void {
        ++$this->ITERATEABLE_POSITION;
    }

    public function rewind(): void {
        $this->ITERATEABLE_POSITION = 0;
    }

    public function valid(): bool {
        return $this->has($this->ITERATEABLE_KEYS[$this->ITERATEABLE_POSITION]);
    }

}
