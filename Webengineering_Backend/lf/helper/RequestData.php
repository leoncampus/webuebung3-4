<?php

namespace lf\helper;

class RequestData {
    private $obj = "";
    private $method = "";
    private $data = "";
    private $session = "";
    
    public function __construct(\stdClass $jsonIn, array $headers) {
        $jsonIn = $jsonIn->jsonReq;

        if(isset($headers["Authorization"])) {
            $this->session = str_replace("JWT ","",$headers["Authorization"]);
        }else{
            $this->session = '';
        }
        
        if($jsonIn != null) {
            $data = '';
            foreach($jsonIn as $input) {
                if($this->obj == '' && isset($input->o) && $input->o != '') {
                    $this->obj = $input->o;
                    $this->method = $input->m;
                }

                $data = $data.$input->d;
            }
        }

        $this->data = $data;
    }
    
    public function getSession() : string {
        return $this->session;
    }
    
    public function getObject() : string {
        return $this->obj;
    }
    
    public function getMethod() : string {
        return $this->method;
    }
    
    
    public function getData() : string {
        return $this->data;
    }
}
