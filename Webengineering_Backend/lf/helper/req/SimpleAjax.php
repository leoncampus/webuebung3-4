<?php

namespace lh\helper\req;

use lh\helper\JSON;
use lh\contracts\request\Request;

class SimpleAjax extends Request {
    
    public function functionName(JSON $jsonIn) : JSON {
        $json = new JSON();
        
        if($jsonIn->has('text')){
            $json->add('responseText', 'hallo client!');
        }else{
            $json->add('wrong', 'text missing');
        }
        
        
        return $json;
    }
}
