<?php
namespace lf\helper;

class Messages {
    private $msgs = [];
    private $errorCount = 0;
    private $warningCount = 0;
    private $successCount = 0;
    private $infoCount = 0;
        
    public function add(string $type, string $msg, array $fields = []) {
        if($type == "error" || $type == "warning" || $type == "success" || $type == "info") {
            if(strlen($msg) > 0) {
                $json = new JSON();

                if($type == "error") {
                    $this->errorCount++;
                } else if($type == "warning") {
                    $this->warningCount++;
                } else if($type == "success") {
                    $this->successCount++;
                } else if($type == "info") {
                    $this->infoCount++;
                }
                
                $json->add("type" ,$type);
                $json->add("text" ,$msg);
                $json->add("fields", $fields);

                $this->msgs[] = $json;
            } else {
                error_log("helper\Messages.php: No message was given");
            }
        } else {
            error_log("helper\Messages.php: Wrong type");
        }
    }
    
    public function get() : array {
        return $this->msgs;
    }
    
    private function getMsgsByType(string $type) : array {
        $msgs = [];
        
        foreach($this->msgs as $msg) {
            if($msg->getStr("type") == $type) {
                $msgs[] = $msg;
            }
        }
        
        return $msgs;
    }
    
    public function getErrors() : array {
        return getMsgsByType("error");
    }
    public function getWarnings() : array {
        return getMsgsByType("warning");
    }
    public function getSuccesses() : array {
        return getMsgsByType("success");
    }
    public function getInfos() : array {
        return getMsgsByType("info");
    }
    
    public function getErrorCount() {
        return $this->errorCount;
    }
    public function getWarningCount() {
        return $this->warningCount;
    }
    public function getSuccessCount() {
        return $this->successCount;
    }
    public function getInfoCount() {
        return $this->infoCount;
    }
}
