<?php
namespace lf\helper;

class Formatter {
    public static function null2Blank($str) : string {
        if($str === NULL) {
            return "";
        }
        
        return "".$str;
    }
    
    public static function dbDateTime($value) : string {
        $value = self::null2Blank($value);
        
        // date or date time with just period and colon problem
        $date = explode('.', $value);
        if(count($date) === 3) {
            if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 4) {
                // date only
                $value = $date[2] . '-' . $date[1] . '-' . $date[0];
            } else if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 13) {
                // datetime
                $dateAndTime = explode(' ', $date[2]);
                $value = $dateAndTime[0] . '-' . $date[1] . '-' . $date[0] . ' ' . $dateAndTime[1];
            }
        }
        return "".$value;
    }
    
    public static function dbDateTimeSlash($value) : string {
        $value = self::null2Blank($value);
        
        // date or date time with just period and colon problem
        $date = explode('/', $value);
        if(count($date) === 3) {
            if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 4) {
                // date only
                $value = $date[2] . '-' . $date[1] . '-' . $date[0];
            } else if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 13) {
                // datetime
                $dateAndTime = explode(' ', $date[2]);
                $value = $dateAndTime[0] . '-' . $date[1] . '-' . $date[0] . ' ' . $dateAndTime[1];
            }
        }
        
        return "".$value;
    }
    
    public static function dbDateTimeDash($value) : string {
        $value = self::null2Blank($value);
        
        // date or date time with just period and colon problem
        $date = explode('-', $value);
        if(count($date) === 3) {
            if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 4) {
                // date only
                $value = $date[2] . '-' . $date[1] . '-' . $date[0];
            } else if(strlen($date[0]) == 2 && strlen($date[1]) == 2 && strlen($date[2]) == 13) {
                // datetime
                $dateAndTime = explode(' ', $date[2]);
                $value = $dateAndTime[0] . '-' . $date[1] . '-' . $date[0] . ' ' . $dateAndTime[1];
            }
        }
        
        return "".$value;
    }
    
    public static function handleDateTime($value) : string {
        $value = self::null2Blank($value);
        
        if(is_string($value) && $value === "0000-00-00") {
            $value = ""; // Empty date field
        } else if(substr_count($value, '-') == 2 && strlen($value) == 10) {
            // Check if value is date
            $dateCheck = explode('-', $value);
            if(count($dateCheck) == 3) {
                if(strlen($dateCheck[0]) == 4 && strlen($dateCheck[1]) == 2 && strlen($dateCheck[2]) == 2) {
                    // String is date - convert it
                    $value = $dateCheck[2] . '.' . $dateCheck[1] . '.' . $dateCheck[0];
                }
            }
        } else if(substr_count($value, '-') == 2 && substr_count($value, ':') == 2 && strlen($value) == 19) {
            // Check if value is datetime / timestamp
            $dateTime = explode(' ', $value);
            if(count($dateTime) == 2) {
                $dateCheck = explode('-', $dateTime[0]);
                $timeCheck = explode(':', $dateTime[1]);
                if(count($dateCheck) == 3 && count($timeCheck) == 3) {
                    if(strlen($dateCheck[0]) == 4 && strlen($dateCheck[1]) == 2 && strlen($dateCheck[2]) == 2) {
                        // String is datetime - convert it
                        $value = $dateCheck[2] . '.' . $dateCheck[1] . '.' . $dateCheck[0] . ' ' . $dateTime[1];
                    }
                }
            }            
        }
        
        return "".$value;
    }
}
