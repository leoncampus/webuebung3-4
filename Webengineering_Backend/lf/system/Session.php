<?php

namespace lf\system;

use lf\database\DB;
use lf\config\Config;

class Session {
    private $id = 0;
    private $sessionID = "";
    private $email = "";
    private $db;

    public function __construct(string $sessID = '', int $user_id = 0) {
        if(strlen($sessID) > 0) {
            $this->setSessionID($sessID);
            $config = Config::getInstance();
            
        } else {
            error_log('Session empty');
        }
    }

    /* Setters */
    public function setSessionID(string $id) {
        $this->sessionID = $id;
    }
    
    /* Getters */
    public function getSessionID() : string {
        return $this->sessionID;
    }
    
    public function getID() : int {
        return $this->id;
    }
    
    public function getMail() : string {
        return $this->email;
    }
}
