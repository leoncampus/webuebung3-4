-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 06. Okt 2019 um 15:42
-- Server-Version: 10.1.39-MariaDB
-- PHP-Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `webengineering`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lf_sweets`
--

CREATE TABLE `lf_sweets` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `calories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `lf_sweets`
--

INSERT INTO `lf_sweets` (`ID`, `name`, `calories`) VALUES
(1, 'Cake', 340),
(2, 'Sacher Torte', 560),
(3, 'new sweet', 100),
(4, 'new sweet2', 120),
(5, 'Toblerone', 4000),
(6, 'Zuckerl', 100),
(7, 'Schnitte', 200),
(8, 'Neu', 400);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lf_user`
--

CREATE TABLE `lf_user` (
  `ID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `lf_user`
--

INSERT INTO `lf_user` (`ID`, `username`, `password`, `email`) VALUES
(1, 'user', 'pwd', 'email'),
(2, 'user2', '$2y$10$v2dydi8X57x/0c5Z2kjPs.iewjCYbEbp4Lu.567z5g9AnW4bDK9bG', 'user@user.at'),
(3, 'user3', '$2y$10$A5O1l21GrAYoXbz.6KdWjuQqQzuwfxyt5OVK2y137jRthQt9Dq/jy', 'user3@user.at');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `lf_sweets`
--
ALTER TABLE `lf_sweets`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `lf_user`
--
ALTER TABLE `lf_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `lf_sweets`
--
ALTER TABLE `lf_sweets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `lf_user`
--
ALTER TABLE `lf_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
